import React from 'react';
import './App.css';
import { Link } from 'react-router-dom';

function Nav() {
    const navStyle = {
        color: 'white'
    };

    return (
        <nav>
            <h3>Simple Task</h3>
            <ul className="nav-links">
                <Link style={navStyle} to="/posts">
                    <li>Posts</li>
                </Link>
                <Link style={navStyle} to="/photos">
                    <li>Photos</li>
                </Link>
            </ul>
        </nav>
    );
}

export default Nav;