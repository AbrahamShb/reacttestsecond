import React, { Component } from 'react';

class Photos extends Component {
    constructor(props) {
        super(props);
        this.state = { wells: [] }
    }

    getData() {
        fetch(`https://jsonplaceholder.typicode.com/photos?albumId=1`, {
            method: 'Get'
        }).then((response) => {
            response.json().then((response) => {
                this.setState({ wells: response })
            })
        }).catch(err => {
            console.error(err)
        })
    }

    componentDidMount() {
        this.getData()
    }

    getUrl(item) {
        this.setState({
            url: item.url
        })
    }

    render() {
        const { wells } = this.state
        return wells.length ? (
            <div className="container mt-4">
                <div className="row">
                    <div className="col-9">
                        <img src={this.state.url ? this.state.url : wells[0].url} />
                    </div>
                    <div className="col-3 right-block">
                        {
                            wells.map((val, index) => {
                                return (
                                    <div key={index} onClick={(e) => this.getUrl(val)} className="thumb">
                                        <img style={{ width: '100%' }} src={val.thumbnailUrl} />
                                    </div>
                                );
                            })
                        }
                    </div>
                </div>
            </div>
        ) : (
                <span>Loading...</span>
            )

    }
}

export default Photos;