import React, { Component } from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

class Posts extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userId: 1
        }
    }

    getData() {
        fetch(`https://jsonplaceholder.typicode.com/posts`, {
            method: 'Get'
        }).then((response) => {
            response.json().then((response) => {
                this.setState({
                    response
                })
            })
        }).catch(err => {
            console.error(err)
        })
    }

    componentDidMount() {
        this.getData()
    }
    

    render() {
        const columns = [ {
            Header: '№',
            accessor: 'id'
        }, {
            id: 'title',
            Header: 'Title',
            accessor: d => d.title
        },{
            Header: 'user Id',
            accessor: 'userId'
        }];

        const onRowClick = (state, rowInfo, column, instance) => {
            return {
                onClick: e => {     
                    this.props.history.push({pathname:'/subposts', data: rowInfo.original})
                }
            }
        }

        return (
            <div className="container mt-4">
                <ReactTable
                    data={this.state.response}
                    columns={columns}
                    loading={this.state.loading}
                    defaultPageSize={10}
                    className="-striped -highlight"
                    showPaginationBottom
                    getTrProps={onRowClick}
                />
            </div>
        )
        
    }
}

export default Posts;