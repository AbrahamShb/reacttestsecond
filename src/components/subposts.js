import React, { Component } from 'react';
import '../App.css';

class Subposts extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        const { data } = this.props.location
        return (
            <div className="container">
                <div className="row">
                    <div className="col-12 text-center form-group title-block">
                        <h1>{data.title}</h1>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 text-center form-group body-block">
                        <h1>{data.body}</h1>
                    </div>
                </div>
            </div>
        )
    }
}

export default Subposts;