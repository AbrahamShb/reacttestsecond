import React from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Nav from './Nav';
import Posts from './components/posts';
import Photos from './components/photos';
import Subposts from './components/subposts';

function App() { 
    return (
      <Router>
        <div className="App">
          <Nav />
          <Switch>
            <Route exact path = "/" component = {Posts} />
            <Route path = "/posts" component = {Posts} />
            <Route path = "/photos" component = {Photos} />
            <Route path = "/subposts" component = {Subposts} />
          </Switch>
        </div>
      </Router>
    );
}

export default App;